<p align="center">
  <img src="https://docs.nestjs.com/assets/logo-small.svg" width="150px" height="150px"/>
</p>

## NestJS Approov Extended Example

This NestJS implementation provides an example of adding an Approov token check to API processing.

The example application is derived from [NestJS course task management](https://github.com/arielweinberger/nestjs-course-task-management) by Ariel Weinberger. It provides a simple tasks service for loggedin users.

The example code has been modified in the following ways:

1) It uses an SGLite DB instead of Postgres to keep installation simple.

2) The User entity is modified to ad a list of roles the user may have.

3) An Approov token checking module is provided which by default guards all API calls.

## Approov Integration

### Example Database

The example database, `db-EXAMPLE.sqlite`, has two users pre-assigned:

| username | password     | roles   |
| -------- | ------------ | ------- |
| alice    | qEH2X6pT8yYE |         |
| approov  | shPyYp9ynTIh | approov |

To start with the default users, copy the `db-example.sqlite` file to `db.sqlite`.

### ENV Files

ENV files are of the form `.env.stage.<STAGE>`, where `<STAGE>` is typically a value like `dev` or `production`.

The `.env.stage.EXAMPLE` provides a sample configuration. For development, copy it into `.env.stage.dev` and modify the parameters:

```
PORT=3000
PRIVATE_PORT=3001

DB_NAME=db.sqlite

JWT_SECRET=B[T@_6_-M2ux\^u),<7D9hsu99x.2-}bX_2bUXgnW?#5YT*cn$d{HjvBW^#Jfs]j

# Approov secret in base64url format
APPROOV_SECRET=U0VDUkVU

# Approov mode (Active, Passive, or Disabled)
APPROOV_MODE=active
```

Two values are added for Approov:

The `APPROOV_SECRET` is the signing secret in `base64url` notation. It should match the value set for your account, which can be retreived using the Approov CLI:

```
> approov secret -get base64url
``` 

The `APPROOV_MODE` specifies what happens if an Approov token fails the JWT token check. It should be set to one of these properties:

- `active`: block the API call and return a status of `forbidden`.
- `passive`: complete the API call normally but log the token failure.
- `disabled`: ignore the Approov token and complete the API call normally.

The normally operating value should be `active`, but `passive` is useful when first bringing up the Approov infrastructure to check Approov attestation is performing as expected without actually blocking any API calls.

### The Approov Module

Add the Approov module to the top-level app module. For the included `AppModule` example, add the `ApproovModule` to the module's `imports`:

```js
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.stage.${process.env.STAGE}`],
      validationSchema: configValidationSchema,
    }),
    TasksModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const isProduction = configService.get('STAGE') === 'prod';

        return {
          type: 'sqlite', //'postgres',
          synchronize: true,
          database: configService.get('DB_NAME'),
          entities: [Task, User],
        };
      },
    }),
    AuthModule,
    ApproovModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ApproovGuard,
    },
  ],
})
export class AppModule {}
```

### Approov Guards

Once the `ApproovModule` has been imported into the `AppModule`, all routes are checked for a valid Approov token by default.

To exclude any route from Approov token checking, add the `@NoApproovGuard()` decorator to the handler, for example:

```js
@Get('/:id')
@NoApproovGuard()
getTaskById(@Param('id') id: string, @GetUser() user: User): Promise<Task> {
  return this.tasksService.getTaskById(id, user);
}
```

### Approov Secert and Mode Changes

Two endpoints are provided to change the Approov secret and mode:

- `PATCH /approov/secret`

Provide the new secret, form-encoded in the body in `base64url` format:

```
secret: <base64url value>
```

- `PATCH /approov/mode`

Provide the new mode, form-encoded in the body:

```
mode: <mode value>
```

These routes require a signed-in user which has an `approov` role. These routes are not guarded by approov token checks.

## Getting started

1. Run `yarn install`.
2. Copy the `.env.stage.EXAMPLE` file into the `.env.stage.dev` file and adjust as needed.
3. COpy the `db-EXAMPLE.sqlite` file into the `db.sqlite` file to start with default users.
3. Run `yarn start:dev` and confirm there are no errors.
4. Start testing the API...

## Testing the API

Run some unit tests for task setting and the Approov module by running:

```
> yarn test:watch
```

There is a collection of API test calls for the postman-like Thunder Client extension within VSCode. Make sure the `Thunder-client: Save To Workspace` is set so project collections are automatically stored/loaded.

These calls were used to exercise the end to end operation. To set the user access token, login as `alice` and copy the returned jwt token into the /auth/test authorization bearer token.

You can modify the approov and user access tokens to verify that an API call must first pass the Approov check and then must pass the user auth check.

Automated end-to-end tests still need to be added.

---
