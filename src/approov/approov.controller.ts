import {
  Body,
  Controller,
  Get,
  Logger,
  Patch,
  UseGuards,
} from '@nestjs/common';
import { ApproovService } from './approov.service';
import { NoApproovGuard } from './approov.guard';
import { ApproovModeDto } from './dto/approov-mode.dto';
import { ApproovSecretDto } from './dto/approov-secret.dto';
import { UserGuard } from '../auth/user.guard';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from 'src/auth/roles.decorator';

@Controller('/approov')
export class ApproovController {
  private logger = new Logger('ApproovController');

  constructor(private approovService: ApproovService) {}

  // consider removing this functionality for production
  @Get('/secret')
  @Roles('approov')
  @NoApproovGuard()
  @UseGuards(UserGuard, RolesGuard)
  getSecret(): string {
    this.logger.log('Getting Approov secret');
    return this.approovService.getSecret();
  }

  @Patch('/secret')
  @Roles('approov')
  @NoApproovGuard()
  @UseGuards(UserGuard, RolesGuard)
  patchSecret(@Body() approovSecretDto: ApproovSecretDto): void {
    const { secret } = approovSecretDto;
    this.approovService.setSecret(secret);
  }

  // consider removing this functionality for production
  @Get('/mode')
  @Roles('approov')
  @NoApproovGuard()
  @UseGuards(UserGuard, RolesGuard)
  getMode(): string {
    this.logger.log('Getting Approov mode');
    return this.approovService.getMode();
  }

  @Patch('/mode')
  @Roles('approov')
  @NoApproovGuard()
  @UseGuards(UserGuard, RolesGuard)
  patchMode(@Body() approovModeDto: ApproovModeDto): void {
    const { mode } = approovModeDto;
    this.approovService.setMode(mode);
  }
}
