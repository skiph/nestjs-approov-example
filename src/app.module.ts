import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TasksModule } from './tasks/tasks.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ApproovGuard } from './approov/approov.guard';
import { ApproovModule } from './approov/approov.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configValidationSchema } from './config.schema';
import { Task } from './tasks/task.entity';
import { User } from './auth/user.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.stage.${process.env.STAGE}`],
      validationSchema: configValidationSchema,
    }),
    TasksModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const isProduction = configService.get('STAGE') === 'prod';

        return {
          // ssl: isProduction,
          // extra: {
          //   ssl: isProduction ? { rejectUnauthorized: false } : null,
          // },
          type: 'sqlite', //'postgres',
          // autoLoadEntities: true,
          synchronize: true,
          // host: configService.get('DB_HOST'),
          // port: configService.get('DB_PORT'),
          // username: configService.get('DB_USERNAME'),
          // password: configService.get('DB_PASSWORD'),
          database: configService.get('DB_NAME'), // configService.get('DB_DATABASE'),
          entities: [Task, User],
        };
      },
    }),
    AuthModule,
    ApproovModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ApproovGuard,
    },
  ],
})
export class AppModule {}
