import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { ApproovService } from './approov.service';
import { ApproovController } from './approov.controller';
import { ApproovGuard } from './approov.guard';

@Global()
@Module({
  imports: [ConfigModule],
  controllers: [ApproovController],
  providers: [
    ApproovService,
    {
      provide: APP_GUARD,
      useClass: ApproovGuard,
    },
  ],
  exports: [ApproovService],
})
export class ApproovModule {}
