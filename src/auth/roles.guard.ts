import {
  Injectable,
  CanActivate,
  ExecutionContext,
  NotFoundException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { Roles } from './roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  private matchRoles(roles: string[], userRoles: string[]): boolean {
    let matched = true;
    roles.forEach((role) => {
      if (!userRoles.includes(role)) {
        matched = false;
      }
    });

    return matched;
  }

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user) {
      return true;
    }
    const userRoles = !user?.roles.trim() ? [] : user.roles.split(',');

    if (this.matchRoles(roles, userRoles)) {
      return true;
    }

    // consider throwing 404 exception here to hide unauthorized route
    // throw new NotFoundException();
    return false;
  }
}
