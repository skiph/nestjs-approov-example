import {
  CanActivate,
  ExecutionContext,
  Inject,
  Logger,
  SetMetadata,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ApproovService } from './approov.service';

export class ApproovGuard implements CanActivate {
  private logger = new Logger('ApproovGuard');

  constructor(
    @Inject(ApproovService) private approovService: ApproovService,
    private reflector: Reflector,
  ) {}

  canActivate(context: ExecutionContext) {
    const noGuard = this.reflector.get<boolean>(
      'noApproovGuard',
      context.getHandler(),
    );
    if (noGuard) {
      return true;
    }

    const req = context.switchToHttp().getRequest();

    const claims = this.approovService.verify(req);

    if (claims) {
      req.approovTokenClaims = claims;
      return true;
    }

    return false;
  }
}

export const NoApproovGuard = () => SetMetadata('noApproovGuard', true);
