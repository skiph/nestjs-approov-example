import { Test } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import base64url from 'base64url';
import { ApproovService } from './approov.service';
import { sign } from 'jsonwebtoken';

const SECRET = base64url.encode('SECRET');
const OTHER_SECRET = base64url.encode('OTHER_SECRET');

describe('ApproovService', () => {
  let approovService: ApproovService;

  const FakeConfigService: Partial<ConfigService> = {
    get: (name) => {
      if (name === 'APPROOV_SECRET') return 'U0VDUkVU';
      if (name === 'APPROOV_MODE') return 'active';
      return '';
    },
  };

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        ApproovService,
        {
          provide: ConfigService,
          useValue: FakeConfigService,
        },
      ],
    }).compile();

    approovService = module.get(ApproovService);
  });

  it('can create an instance of the approov service', async () => {
    expect(approovService).toBeDefined();
  });

  it('returns the secret', async () => {
    const secret: string = approovService.getSecret();
    expect(secret).toEqual(SECRET);
  });

  it('changes the secret', async () => {
    approovService.setSecret(OTHER_SECRET);
    expect(approovService.getSecret()).toEqual(OTHER_SECRET);
  });

  it('returns the mode', async () => {
    const mode = approovService.getMode();
    expect(mode).toEqual('active');
  });

  it('changes the mode', async () => {
    const mode = 'passive';
    approovService.setMode(mode);
    expect(approovService.getMode()).toEqual(mode.toLowerCase());
  });

  it('validates a request with a properly signed live Approov token in active mode', async () => {
    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
          },
          base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('invalidates a request with a properly signed but expired Approov token in active mode', async () => {
    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) - 60 * 60,
          },
          base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeFalsy();
  });

  it('invalidates a request with an improperly signed live Approov token in active mode', async () => {
    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
          },
          'NOT_' + base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeFalsy();
  });

  it('invalidates a request with no Approov token in active mode', async () => {
    const request = {
      headers: {},
    };
    expect(approovService.verify(request)).toBeFalsy();
  });

  it('validates a request with a properly signed live Approov token in passive mode', async () => {
    approovService.setMode('passive');

    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
          },
          base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with a properly signed but expired Approov token in active mode', async () => {
    approovService.setMode('passive');

    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) - 60 * 60,
          },
          base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with an improperly signed live Approov token in active mode', async () => {
    approovService.setMode('passive');

    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
          },
          'NOT_' + base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with no Approov token in passive mode', async () => {
    approovService.setMode('passive');

    const request = {
      headers: {},
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with a properly signed live Approov token in disabled mode', async () => {
    approovService.setMode('disabled');

    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
          },
          base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with a properly signed but expired Approov token in disabled mode', async () => {
    approovService.setMode('disabled');

    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) - 60 * 60,
          },
          base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with an improperly signed live Approov token in disabled mode', async () => {
    approovService.setMode('disabled');

    const request = {
      headers: {
        'approov-token': sign(
          {
            TestClaim: 'tESTcLAIM',
            exp: Math.floor(Date.now() / 1000) + 60 * 60,
          },
          'NOT_' + base64url.decode(approovService.getSecret()),
        ),
      },
    };
    expect(approovService.verify(request)).toBeTruthy();
  });

  it('validates a request with no Approov token in disabled mode', async () => {
    approovService.setMode('disabled');

    const request = {
      headers: {},
    };
    expect(approovService.verify(request)).toBeTruthy();
  });
});
