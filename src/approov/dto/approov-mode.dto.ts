import { IsString, Matches } from 'class-validator';

export class ApproovModeDto {
  @IsString()
  @Matches(/([Aa]ctive)|([Pp]assive)|([Dd]isabled)/, {
    message: 'mode is one of Active, Passive, or Disabled',
  })
  mode: string;
}
