import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import base64url from 'base64url';
import { verify } from 'jsonwebtoken';
import { ApproovPayload } from './approov-payload.interface';

@Injectable()
export class ApproovService {
  private logger = new Logger('ApproovService');
  private secret: string;
  private mode: string;

  private HeaderName = 'approov-token';

  private extractToken(request: { [key: string]: any }): string {
    return request?.headers?.[this.HeaderName] || '';
  }

  constructor(private configService: ConfigService) {
    const secret = this.configService.get('APPROOV_SECRET');
    this.secret = base64url.decode(secret);
    this.mode = this.configService.get('APPROOV_MODE');
  }

  setMode(mode: string): void {
    this.mode = mode.toLowerCase();
  }

  getMode(): string {
    return this.mode;
  }

  setSecret(secret: string /* base64url */): void {
    this.secret = base64url.decode(secret);
  }

  getSecret(): string /* base64url */ {
    return base64url.encode(this.secret);
  }

  verify(request: { [key: string]: any }): ApproovPayload | null {
    if (this.mode === 'disabled') return {};

    let claims = {};
    try {
      const token = this.extractToken(request);
      if (!token) {
        if (this.mode === 'passive') {
          this.logger.warn('Invalid API Request: no Approov token found');
          return {};
        } else {
          this.logger.warn('Invalid API Request: no Approov token found');
          return null;
        }
      }

      const claims = verify(token, this.secret, {
        algorithms: ['HS256'],
      });
      return claims;
    } catch (e) {
      if (this.mode === 'passive') {
        this.logger.warn('Invalid API Request: invalid Approov token');
        return {};
      } else {
        this.logger.warn('Invalid API Request: invalid Approov token');
        return null;
      }
    }
  }
}
