import { IsString, MinLength } from 'class-validator';

export class ApproovSecretDto {
  @IsString()
  @MinLength(6)
  secret: string;
}
