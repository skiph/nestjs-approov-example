import * as Joi from '@hapi/joi';

export const configValidationSchema = Joi.object({
  PORT: Joi.number().default(3000),
  STAGE: Joi.string().required(),
  DB_NAME: Joi.string().required(),
  JWT_SECRET: Joi.string().required(),

  APPROOV_SECRET: Joi.string().required(),
  APPROOV_MODE: Joi.string().default('enabled'),
});
